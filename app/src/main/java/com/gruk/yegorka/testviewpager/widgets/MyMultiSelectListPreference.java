package com.gruk.yegorka.testviewpager.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.preference.MultiSelectListPreference;
import android.util.AttributeSet;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Egor on 06.01.2016.
 */
public class MyMultiSelectListPreference extends MultiSelectListPreference {

    public MyMultiSelectListPreference(Context context) {
        super(context);
    }

    public MyMultiSelectListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MyMultiSelectListPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MyMultiSelectListPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void setValues(Set<String> values) {
        //Workaround for https://code.google.com/p/android/issues/detail?id=22807
        final Set<String> newValues = new HashSet<String>();
        newValues.addAll(values);

        super.setValues(newValues);
    }
}
