package com.gruk.yegorka.testviewpager.fragments;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.widget.Toast;

import com.gruk.yegorka.testviewpager.App;
import com.gruk.yegorka.testviewpager.R;
import com.gruk.yegorka.testviewpager.Util;
import com.gruk.yegorka.testviewpager.activities.MainActivity;
import com.gruk.yegorka.testviewpager.database.MyPrefs;
import com.gruk.yegorka.testviewpager.database.MySQLiteClass;
import com.gruk.yegorka.testviewpager.database.MySQLiteClassHolder;
import com.gruk.yegorka.testviewpager.models.NotificationModel;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PrefsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName(MyPrefs.APP_PREFS);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        Toast.makeText(getActivity(), "key - " + preference.getKey() + " title - " + preference.getTitle(), Toast.LENGTH_LONG).show();
        if (preference.getKey().equals(Util.getString(R.string.update_now))) {
            ExecutorService service = Executors.newFixedThreadPool(1);
            service.execute(new Async());
            service.shutdown();
        }

        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    class Async implements Runnable {

        private NotificationModel notificationModel;

        public Async() {

            notificationModel = new NotificationModel();

            notificationModel
                    .setClazz(MainActivity.class)
                    .setContentTitle(R.string.notification_update_start)
                    .setContentText(R.string.notification_update_start_text);
        }

        @Override
        public void run() {

            Util.sendNotification(notificationModel);

            MySQLiteClass liteClass = MySQLiteClassHolder.getInstance();//new MySQLiteClass(getActivity());
            liteClass.initialFillingSafe(getResources());

            notificationModel
                    .setContentTitle(R.string.notification_update_finished)
                    .setContentText(R.string.notification_update_finished_text);

            Util.sendNotification(notificationModel);

        }
    }
}