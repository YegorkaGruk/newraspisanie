package com.gruk.yegorka.testviewpager.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gruk.yegorka.testviewpager.R;
import com.gruk.yegorka.testviewpager.Util;
import com.gruk.yegorka.testviewpager.activities.MainActivity;
import com.gruk.yegorka.testviewpager.activities.ScheduleActivity;
import com.gruk.yegorka.testviewpager.database.MyPrefs;
import com.gruk.yegorka.testviewpager.models.NotificationModel;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;
import com.rey.material.app.Dialog;

import java.util.ArrayList;
import java.util.Arrays;

public class FragmentStationSingle extends Fragment implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener {

    public static final String TITLE = "По станции";//Util.getString(R.string.by_station);

    private static final String KEY = FragmentStationSingle.class.getName();

    private MaterialAutoCompleteTextView editStationSingle;
    private ProgressBar progressBar;

    ViewPager viewPager;
    ListView listView;
    ArrayAdapter<String> adapterRecent, adapter;

    public FragmentStationSingle() {
    }

    public static Fragment newInstance(int pos) {
        FragmentStationSingle fragment = new FragmentStationSingle();
        Bundle args = new Bundle();
        args.putInt(KEY, pos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        viewPager = (ViewPager) container;
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_spinner);

        View view = inflater.inflate(R.layout.fragments_stations_single, container, false);

        listView = (ListView) view.findViewById(R.id.list_view_recent);

        adapterRecent = new ArrayAdapter<>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                //new ArrayList<>(MyPrefs.getInstance().getStringSet(KEY))
                new ArrayList<>(MyPrefs.getInstance().getList(KEY))
        );


        listView.setAdapter(adapterRecent);

        listView.setLongClickable(true);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);

        editStationSingle = (MaterialAutoCompleteTextView) view.findViewById(R.id.editText_one);

        final String[] stations = MyPrefs.getInstance().getStringPrefs(MyPrefs.STATION_PREFS).split(MyPrefs.DIVIDER);
        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, stations);

        editStationSingle.setAdapter(adapter);

        Button button = (Button) view.findViewById(R.id.button_show_rasp);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String stationSingle = editStationSingle.getText().toString();

                if (Arrays.asList(stations).contains(stationSingle)) {
                    //MyPrefs.getInstance().addStringSet(KEY, stationSingle);
                    MyPrefs.getInstance().add2List(KEY, stationSingle);
                    startActivity(stationSingle);
                } else {

                    if (stationSingle.equals(""))
                        editStationSingle.setError("Введите название станции");
                    else
                        editStationSingle.setError("Нет такой станции \"" + stationSingle + "\"");
                }
            }
        });

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume", "FragmentStationSingle.onResume");
        notifyChanges();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //String data = (String) parent.getItemAtPosition(position);
        //startActivity(data);
        NotificationModel notificationModel = new NotificationModel();

        notificationModel
                .setContext(getContext())
                .setClazz(MainActivity.class)
                .setTicker("setTicker")
                .setContentTitle(R.string.notification_update_start)
                .setContentText(R.string.notification_update_start_text);

        Log.e("onItemClick", notificationModel.toString());

        Util.sendNotification(notificationModel);
        //Util.sendNotification(MainActivity.class);

    }

    @Override
    public boolean onItemLongClick(final AdapterView<?> parent, View view, final int position, long id) {

        final Dialog mDialog = new Dialog(getActivity());

        mDialog
                .title(R.string.dialog_title)
                .positiveAction(R.string.dialog_positive)
                .positiveActionClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String data = (String) parent.getItemAtPosition(position);
                        //MyPrefs.getInstance().removeStringSet(KEY, data);
                        MyPrefs.getInstance().removeFromList(KEY, data);
                        notifyChanges();
                        mDialog.dismiss();
                        Toast.makeText(getContext(), "Confirmed", Toast.LENGTH_SHORT).show();
                    }
                })
                .negativeAction(R.string.dialog_negative)
                .negativeActionClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                        mDialog.dismiss();
                    }
                })
                .cancelable(true)
                .show();

        return true;
    }

    private void startActivity(String extra) {
        Intent intent = new Intent(getActivity(), ScheduleActivity.class);
        intent.putExtra(MyPrefs.STATION_FROM, extra);
        startActivity(intent);
    }

    private void notifyChanges() {
        adapterRecent.clear();
        //adapterRecent.addAll(MyPrefs.getInstance().getStringSet(KEY));
        adapterRecent.addAll(MyPrefs.getInstance().getList(KEY));
    }

}