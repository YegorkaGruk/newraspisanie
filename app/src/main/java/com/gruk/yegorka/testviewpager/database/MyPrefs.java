package com.gruk.yegorka.testviewpager.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.gruk.yegorka.testviewpager.App;
import com.gruk.yegorka.testviewpager.Util;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class MyPrefs {

    public static final String DIVIDER = ",";
    public static final String DIVIDER2 = "@";//stations
    public static final String APP_PREFS = "app_prefs";
    public static final String STATION_PREFS = "station_prefs";
    public static final String UPDATE_DATE = "update_date";
    public static final String STATION_FROM = "station_from";
    public static final String STATION_TO = "station_to";
    public static final String STATION_SET = "station_set";

    private static final String TAG = MyPrefs.class.getName();
    private static volatile MyPrefs instance;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public MyPrefs() {
        preferences = App.getContext().getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public static MyPrefs getInstance() {
        MyPrefs localInstance = instance;
        if (localInstance == null) {
            synchronized (MyPrefs.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new MyPrefs();
                }
            }
        }
        return localInstance;
    }

    public void setPrefs(String key, boolean value) {
        //SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void setPrefs(String key, String value) {
        //SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public boolean getBoolPrefs(String key) {
        return preferences.getBoolean(key, false);
    }

    public String getStringPrefs(String key) {
        return preferences.getString(key, "");
    }

    /*
    public Set<String> getStringSet(String key) {
        return preferences.getStringSet(key, new LinkedHashSet<String>());
    }

    public void setStringSet(String key, Set<String> set) {
        log("до установки");
        log(getStringSet(key));
        preferences.edit().remove(key).apply();
        log("после remove");
        log(getStringSet(key));
        log("из funnyAdd");
        log(set);
        preferences.edit().putStringSet(key, set).apply();
        log("после установки");
        log(getStringSet(key));
    }

    public void addStringSet(String key, String string) {

        Set<String> set = getStringSet(key);
        log("до добавления");
        log(getStringSet(key));

        set = Util.funnyAdd(set, string);
        log("после funnyAdd");
        log(set);

        setStringSet(key, set);
        log("после добавления");
        log(getStringSet(key));
    }

    public void removeStringSet(String key, String string) {
        Set<String> set = new LinkedHashSet<>(getStringSet(key));
        set.remove(string);
        setStringSet(key, set);
    }
    */

    public void setList(String key, List<String> list) {
        editor.putString(key, new JSONArray(list).toString());
        editor.apply();
    }

    public List<String> getList(String key) {

        String JSONString = preferences.getString(key, null);

        if (JSONString == null)
            return new LinkedList<>();

        try {
            return Util.JSONArray2List(new JSONArray(JSONString));
        } catch (JSONException e) {
            throw new RuntimeException();
        }

    }

    public void add2List(String key, String string) {
        LinkedList<String> list = new LinkedList<>(getList(key));

        if (list.contains(string))
            list.remove(string);

        list.addFirst(string);
        setList(key, list);
    }

    public void removeFromList(String key, String string) {
        List<String> list = new LinkedList<>(getList(key));
        list.remove(string);
        setList(key, list);
    }


    private void log(String s) {
        Log.e(TAG, s);
    }

    private void log(Set<String> set) {
        for (String s : set) {
            Log.d(TAG, s);
        }
    }

}
