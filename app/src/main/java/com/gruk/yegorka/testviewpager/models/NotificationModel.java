package com.gruk.yegorka.testviewpager.models;

import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.gruk.yegorka.testviewpager.App;
import com.gruk.yegorka.testviewpager.R;
import com.gruk.yegorka.testviewpager.Util;

/**
 * Created by Egor on 16.01.2016.
 */
public class NotificationModel {

    private Class clazz;
    private String contentTitle;
    private String contentText;
    private Context context;

    private int requestCode;
    private boolean autoCancel;
    private String ticker;
    @DrawableRes
    private int smallIcon;
    private boolean ongoing;
    private String subText;
    private int number;
    private long when;
    private int id;
    private Uri soundUri;

    public NotificationModel() {
        defaultInit();
    }

    public NotificationModel(Context context, Class clazz, String contentTitle, String contentText) {
        this.context = context;
        this.clazz = clazz;
        this.contentTitle = contentTitle;
        this.contentText = contentText;

        defaultInit();
    }

    public NotificationModel(Context context, Class clazz, @StringRes int contentTitle, @StringRes int contentText) {
        this.context = context;
        this.clazz = clazz;
        this.contentTitle = Util.getString(contentTitle);
        this.contentText = Util.getString(contentText);

        defaultInit();
    }

    private void defaultInit() {
        context = App.getContext();
        requestCode = 0;
        autoCancel = true;
        ticker = "";
        smallIcon = R.drawable.ic_train;
        ongoing = false;
        subText = "";
        number = 1;
        when = System.currentTimeMillis();
        id = 1;
        soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    }

    public Class getClazz() {
        return clazz;
    }

    public NotificationModel setClazz(Class clazz) {
        this.clazz = clazz;
        return this;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public NotificationModel setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
        return this;
    }

    public NotificationModel setContentTitle(@StringRes int contentTitle) {
        this.contentTitle = Util.getString(contentTitle);
        return this;
    }

    public String getContentText() {
        return contentText;
    }

    public NotificationModel setContentText(String contentText) {
        this.contentText = contentText;
        return this;
    }

    public NotificationModel setContentText(@StringRes int contentText) {
        this.contentText = Util.getString(contentText);
        return this;
    }

    public Context getContext() {
        return context;
    }

    public NotificationModel setContext(Context context) {
        this.context = context;
        return this;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public NotificationModel setRequestCode(int requestCode) {
        this.requestCode = requestCode;
        return this;
    }

    public boolean isAutoCancel() {
        return autoCancel;
    }

    public NotificationModel setAutoCancel(boolean autoCancel) {
        this.autoCancel = autoCancel;
        return this;
    }

    public String getTicker() {
        return ticker;
    }

    public NotificationModel setTicker(String ticker) {
        this.ticker = ticker;
        return this;
    }

    public int getSmallIcon() {
        return smallIcon;
    }

    public NotificationModel setSmallIcon(int smallIcon) {
        this.smallIcon = smallIcon;
        return this;
    }

    public boolean isOngoing() {
        return ongoing;
    }

    public NotificationModel setOngoing(boolean ongoing) {
        this.ongoing = ongoing;
        return this;
    }

    public String getSubText() {
        return subText;
    }

    public NotificationModel setSubText(String subText) {
        this.subText = subText;
        return this;
    }

    public int getNumber() {
        return number;
    }

    public NotificationModel setNumber(int number) {
        this.number = number;
        return this;
    }

    public long getWhen() {
        return when;
    }

    public NotificationModel setWhen(long when) {
        this.when = when;
        return this;
    }

    public int getId() {
        return id;
    }

    public NotificationModel setId(int id) {
        this.id = id;
        return this;
    }

    public Uri getSoundUri() {
        return soundUri;
    }

    public NotificationModel setSoundUri(Uri soundUri) {
        this.soundUri = soundUri;
        return this;
    }

    @Override
    public String toString() {
        return "NotificationModel{" +
                "\nclazz=" + clazz +
                "\n, contentTitle='" + contentTitle + '\'' +
                "\n, contentText='" + contentText + '\'' +
                "\n, context=" + context +
                "\n, requestCode=" + requestCode +
                "\n, autoCancel=" + autoCancel +
                "\n, ticker='" + ticker + '\'' +
                "\n, smallIcon=" + smallIcon +
                "\n, ongoing=" + ongoing +
                "\n, subText='" + subText + '\'' +
                "\n, number=" + number +
                "\n, when=" + when +
                "\n, id=" + id +
                '}';
    }
}
