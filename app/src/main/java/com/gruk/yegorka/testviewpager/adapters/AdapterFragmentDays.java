package com.gruk.yegorka.testviewpager.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.gruk.yegorka.testviewpager.fragments.FragmentDayEveryDay;
import com.gruk.yegorka.testviewpager.fragments.FragmentDayToday;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class AdapterFragmentDays extends FragmentPagerAdapter {

    public AdapterFragmentDays(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FragmentDayToday.newInstance(position + 1);
            case 1:
                return FragmentDayEveryDay.newInstance(position + 1);
        }
        return null;
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return FragmentDayToday.TITLE;
            case 1:
                return FragmentDayEveryDay.TITLE;
        }
        return null;
    }
}
