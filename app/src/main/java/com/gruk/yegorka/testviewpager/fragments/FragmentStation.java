package com.gruk.yegorka.testviewpager.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;

import com.gruk.yegorka.testviewpager.R;
import com.gruk.yegorka.testviewpager.activities.ScheduleActivity;
import com.gruk.yegorka.testviewpager.database.MyPrefs;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

import java.util.Arrays;
import java.util.List;

public abstract class FragmentStation extends Fragment {

    public static final int STATIONS = "STATIONS".hashCode();
    public static final int ARRAY_ADAPTER = "ARRAY_ADAPTER".hashCode();

    private ProgressBar progressBar;

    @LayoutRes
    protected abstract int getLayoutResId();

    protected abstract void onButtonClick();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_spinner);

        final View view = inflater.inflate(getLayoutResId(), container, false);

        final String[] stations = MyPrefs.getInstance().getStringPrefs(MyPrefs.STATION_PREFS).split(MyPrefs.DIVIDER);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, stations);

        view.setTag(STATIONS, stations);
        view.setTag(ARRAY_ADAPTER, adapter);

        Button button = (Button) view.findViewById(R.id.button_show_rasp);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonClick();
            }
        });

        return view;
    }

    protected void setErrorTo(MaterialAutoCompleteTextView edit) {
        if (edit.getText().toString().equals(""))
            edit.setError("Введите название станции");
        else
            edit.setError("Нет такой станции \"" + edit.getText().toString() + "\"");
    }

    public void switchVisibility() {
        if (progressBar.getVisibility() == View.INVISIBLE)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.INVISIBLE);
    }

    public void startActivity(String s1, String... strings) {

        Intent intent = new Intent(getActivity(), ScheduleActivity.class);

        intent.putExtra(MyPrefs.STATION_FROM, s1);

        if (strings != null)
            intent.putExtra(MyPrefs.STATION_TO, strings[0]);

        startActivity(intent);
    }

}
