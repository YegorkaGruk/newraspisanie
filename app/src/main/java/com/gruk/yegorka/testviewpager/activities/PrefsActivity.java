package com.gruk.yegorka.testviewpager.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;

import com.gruk.yegorka.testviewpager.fragments.PrefsFragment;
import com.gruk.yegorka.testviewpager.widgets.ActionableToastBar;

public class PrefsActivity  extends AppCompatActivity implements View.OnTouchListener {

    private View mUndoFrame;
    private ActionableToastBar mUndoBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new PrefsFragment()).commit();
    }

    @Override
    public void onPause() {
        super.onPause();
        hideUndoBar(false, null);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        hideUndoBar(true, event);
        return false;
    }

    private void hideUndoBar(boolean animate, MotionEvent event) {
        if (mUndoBar != null) {
            mUndoFrame.setVisibility(View.GONE);
            if (event != null && mUndoBar.isEventInToastBar(event)) {
                // Avoid touches inside the undo bar.
                return;
            }
            mUndoBar.hide(animate);
        }
    }

}
