package com.gruk.yegorka.testviewpager;

import android.app.Application;
import android.content.Context;

import com.gruk.yegorka.testviewpager.database.MySQLiteClassHolder;

/**
 * Created by Egor on 31.12.2015.
 */
public class App extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        mContext = this;
        MySQLiteClassHolder.getInstance().open(true);
        super.onCreate();
    }

    public static Context getContext(){
        return mContext;
    }

    @Override
    public void onTerminate() {
        MySQLiteClassHolder.getInstance().close();
        super.onTerminate();
    }
}
