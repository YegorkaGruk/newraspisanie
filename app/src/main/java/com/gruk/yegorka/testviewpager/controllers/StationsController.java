package com.gruk.yegorka.testviewpager.controllers;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.gruk.yegorka.testviewpager.App;
import com.gruk.yegorka.testviewpager.activities.ScheduleActivity;

public class StationsController {

    public static void showSchedule(Context context, String fragmName) {

        Toast.makeText(context, "action from " + fragmName, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(App.getContext(), ScheduleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        App.getContext().startActivity(intent);

    }

}
