package com.gruk.yegorka.testviewpager.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class FragmentDayEveryDay extends FragmentDay {

    public static final String TITLE = "На все дни";

    private static final String KEY = FragmentDayEveryDay.class.getName();

    public FragmentDayEveryDay() {
    }

    public static Fragment newInstance(int pos) {
        FragmentDayEveryDay fragment = new FragmentDayEveryDay();
        Bundle args = new Bundle();
        args.putInt(KEY, pos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected boolean isToday() {
        return false;
    }

    @Override
    protected int getLoaderId() {
        return 1;
    }
}
