package com.gruk.yegorka.testviewpager.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.gruk.yegorka.testviewpager.fragments.FragmentStationDouble;
import com.gruk.yegorka.testviewpager.fragments.FragmentStationSingle;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class AdapterFragmentStations extends FragmentPagerAdapter {

    public AdapterFragmentStations(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a TestPlaceholderFragment (defined as a static inner class below).
        //return TestPlaceholderFragment.newInstance(position + 1);
        switch (position) {
            case 0:
                return FragmentStationDouble.newInstance(position + 1);
            case 1:
                return FragmentStationSingle.newInstance(position + 1);
        }
        return null;
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return FragmentStationDouble.TITLE;
            case 1:
                return FragmentStationSingle.TITLE;
        }
        return null;
    }
}