package com.gruk.yegorka.testviewpager.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gruk.yegorka.testviewpager.R;
import com.gruk.yegorka.testviewpager.models.RecentModel;

import java.util.ArrayList;
import java.util.List;

public class AdapterListRecent extends ArrayAdapter<RecentModel> {

    private Context context;
    private List<RecentModel> models = new ArrayList<>();


    public AdapterListRecent(Context context, List<RecentModel> models) {
        super(context, R.layout.item_listview_recent, models);

        this.models = models;
        this.context = context;
    }

    @Override
    public void notifyDataSetChanged() {
        List<RecentModel> models = new ArrayList<>(this.models);
        this.models.clear();
        this.models.addAll(models);
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.item_listview_recent, parent, false);

        TextView station1 = (TextView) view.findViewById(R.id.station1);
        TextView station2 = (TextView) view.findViewById(R.id.station2);

        Log.d("getView", "getStation1 = " + models.get(position).getStation1() + " | " + "getStation2 = " + models.get(position).getStation2());

        station1.setText(models.get(position).getStation1());
        station2.setText(models.get(position).getStation2());

        return view;
    }

}
