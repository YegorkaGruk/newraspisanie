package com.gruk.yegorka.testviewpager.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gruk.yegorka.testviewpager.R;
import com.gruk.yegorka.testviewpager.activities.ScheduleActivity;
import com.gruk.yegorka.testviewpager.database.MyPrefs;
import com.gruk.yegorka.testviewpager.fragments.FragmentStationDouble;
import com.gruk.yegorka.testviewpager.models.RecentModel;
import com.rey.material.app.Dialog;

import java.util.ArrayList;
import java.util.List;

public class TEST_RVA extends RecyclerView.Adapter<TEST_RVA.ViewHolder> {

    private Context context;
    private List<RecentModel> models = new ArrayList<>();

    public TEST_RVA(Context context, List<RecentModel> models) {
        this.context = context;
        this.models = models;
    }

    public void setModels(List<RecentModel> models) {
        this.models = models;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_item_recent, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.station1.setText(models.get(position).getStation1());
        holder.station2.setText(models.get(position).getStation2());

        holder.tes_cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RecentModel data = models.get(position);
                startActivity(data.getStation1(), data.getStation2());
            }
        });

        holder.tes_cv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final Dialog mDialog = new Dialog(context);

                mDialog

                        .title(R.string.dialog_title)
                        .positiveAction(R.string.dialog_positive)
                        .positiveActionClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                RecentModel data = models.get(position);
                                //MyPrefs.getInstance().removeStringSet(FragmentStationDouble.KEY, data.getStation1() + MyPrefs.DIVIDER2 + data.getStation2());
                                MyPrefs.getInstance().removeFromList(FragmentStationDouble.KEY, data.getStation1() + MyPrefs.DIVIDER2 + data.getStation2());
                                removeItem(position);
                                mDialog.dismiss();
                            }
                        })
                        .negativeAction(R.string.dialog_negative)
                        .negativeActionClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        })
                        .cancelable(true)
                        .show();

                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    private void startActivity(String stationFrom, String stationTo) {
        Intent intent = new Intent(context, ScheduleActivity.class);
        intent.putExtra(MyPrefs.STATION_FROM, stationFrom);
        intent.putExtra(MyPrefs.STATION_TO, stationTo);
        context.startActivity(intent);
    }

    public void removeItem(int position) {
        models.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, models.size());
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        CardView tes_cv;
        TextView station1;
        TextView station2;

        public ViewHolder(View itemView) {
            super(itemView);
            tes_cv = (CardView) itemView.findViewById(R.id.tes_cv);
            station1 = (TextView) itemView.findViewById(R.id.station1);
            station2 = (TextView) itemView.findViewById(R.id.station2);
        }
    }
}
