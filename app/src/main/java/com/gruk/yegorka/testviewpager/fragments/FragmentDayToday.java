package com.gruk.yegorka.testviewpager.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class FragmentDayToday extends FragmentDay{

    public static final String TITLE = "Сегодня";

    private static final String KEY = FragmentDayToday.class.getName();

    public FragmentDayToday() {
    }

    public static Fragment newInstance(int pos) {
        FragmentDayToday fragment = new FragmentDayToday();
        Bundle args = new Bundle();
        args.putInt(KEY, pos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected boolean isToday() {
        return true;
    }

    @Override
    protected int getLoaderId() {
        return 0;
    }

}
