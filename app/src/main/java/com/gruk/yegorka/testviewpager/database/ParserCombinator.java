package com.gruk.yegorka.testviewpager.database;

import android.content.res.Resources;

import com.gruk.yegorka.testviewpager.App;
import com.gruk.yegorka.testviewpager.R;
import com.gruk.yegorka.testviewpager.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserCombinator {

    private final String[] LEFT_PART_PATTERN;
    private final String[] RIGHT_PART_PATTERN;
    private final String DATE_COL;
    private final String SPEC_SIM;

    private String currentMonthName;
    private String currentMonthNum;
    private HashMap<String, String> map;
    private String inputString;
    private String queryString;
    private int currentYear;
    private ArrayList<String[]> parseCases;

    public ParserCombinator() {

        LEFT_PART_PATTERN = Util.getStringArray(R.array.left_part_pattern);
        RIGHT_PART_PATTERN = Util.getStringArray(R.array.right_part_pattern);
        DATE_COL = Util.getString(R.string.date_column);
        SPEC_SIM = Util.getString(R.string.spec_simbol);

        inputString = "";
        queryString = "";

        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        currentYear = cal.get(Calendar.YEAR);

        map = new HashMap<String, String>() {{
            put("пн", "1");
            put("вт", "2");
            put("ср", "3");
            put("чт", "4");
            put("пт", "5");
            put("сб", "6");
            put("вс", "0");
        }};

        Calendar calendar = new GregorianCalendar(new Locale("ru_RU"));

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
        currentMonthNum = dateFormat.format(calendar.getTime());

        dateFormat = new SimpleDateFormat("MMMM", new Locale("ru_RU"));

        currentMonthName = dateFormat.format(calendar.getTime());

        calendar.add(Calendar.MONTH, 1);
        dateFormat = new SimpleDateFormat("MM");


        parseCases = new MyArrayList<String[]>(LEFT_PART_PATTERN.length * 2);

        for (String iterable_element : LEFT_PART_PATTERN) {
            if (iterable_element.contains(SPEC_SIM)) {

                parseCases.add(new String[]{iterable_element.replace(SPEC_SIM, currentMonthName),
                        iterable_element.replace(SPEC_SIM, currentMonthName) + ".*"});
            } else
                parseCases.add(new String[]{iterable_element, iterable_element + ".*"});
        }
        for (String iterable_element : RIGHT_PART_PATTERN) {
            if (iterable_element.contains(SPEC_SIM)) {
                parseCases.add(new String[]{iterable_element.replace(SPEC_SIM, currentMonthName),
                        ".*" + iterable_element.replace(SPEC_SIM, currentMonthName) + ".*"});
            } else
                parseCases.add(new String[]{iterable_element, ".*" + iterable_element + ".*"});
        }
    }

    public String getFormedQuery(String s) {

        if (s.equals("ежедневно"))
            return "";

        resetQueryString();
        setInputString(s);

        for (String[] current : parseCases) {
            if (containsPart(current[1])) {
                getGeneralizedSQL(extractPart(current[0]), current[0], parseCases.indexOf(current));
            }
        }
        return queryString;
    }


    public String getInputString() {
        return inputString;
    }

    public void setInputString(String inputString) {
        this.inputString = inputString;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public void resetQueryString() {
        queryString = "";
    }

    public void resetStrings() {
        inputString = "";
        queryString = "";
    }

    public boolean containsPart(String regExpr) {
        return inputString.matches(regExpr);
    }

    public String extractPart(String regExpr) {
        Pattern pattern = Pattern.compile(regExpr);
        Matcher matcher = pattern.matcher(inputString);
        matcher.find();
        return matcher.group(0);
    }

    public void appendQueryString(String sqlQuery) {
        StringBuilder sb = new StringBuilder(queryString);
        sb.append(sqlQuery);
        this.queryString = sb.toString();
    }

    public void removePart(String regExpr) {
        inputString.replaceAll(regExpr, "");
    }

    public boolean isInputStringEmpty() {
        return inputString.isEmpty();
    }

    public boolean isQueryStringEmpty() {
        return queryString.isEmpty();
    }

    private void getGeneralizedSQL(String partString, String regExpr, int param) {

        String formedString = "";
        Pattern patterm;
        Matcher mather;
        String[] dayAndMounth;
        switch (param) {
            case 0: //getDateSQL
                patterm = Pattern.compile("\\d{2}\\.\\d{2}");
                mather = patterm.matcher(partString);
                while (mather.find()) {
                    dayAndMounth = mather.group().split("\\.");
                    if (formedString.equals("")) {
                        formedString = "(" + DATE_COL + " = \"" + currentYear + "-" + dayAndMounth[1] + "-" + dayAndMounth[0] + "\"";
                    } else {
                        formedString += " OR " + DATE_COL + " = \"" + currentYear + "-" + dayAndMounth[1] + "-" + dayAndMounth[0] + "\"";
                    }
                }
                formedString += ") ";
                break;

            case 1: //getWeekdaySQL
                patterm = Pattern.compile("(пн|вт|ср|чт|пт|сб|вс)+");
                mather = patterm.matcher(partString);
                while (mather.find()) {
                    if (formedString.equals("")) {
                        formedString = "(" + "strftime('%w', " + DATE_COL + ") = \"" + map.get(mather.group()) + "\"";
                    } else {
                        formedString += " OR strftime('%w', " + DATE_COL + ") = \"" + map.get(mather.group()) + "\"";
                    }
                }
                formedString += ")";
                break;

            case 2: //getWeekendsSQL
                formedString = "(strftime('%w', " + DATE_COL + ") = \"0\" OR strftime('%w', " + DATE_COL + ") = \"6\") ";
                break;

            case 3: //getWorkdaysSQL
                formedString = "(strftime('%w', " + DATE_COL + ") != \"0\" AND strftime('%w', " + DATE_COL + ") != \"6\") ";
                break;

            case 4: //getOddDaysSQL
                formedString = "(strftime('%d', " + DATE_COL + ")%2=1) ";
                break;

            case 5: //getEvenDaysSQL
                formedString = "(strftime('%d', " + DATE_COL + ")%2=0) ";
                break;

            case 6: //getAfterDateSQL
                Pattern p3 = Pattern.compile("\\d{1,2}");
                //patterm = Pattern.compile("\\d{2}\\.\\d{2}");
                Matcher m3 = p3.matcher(partString);
                while (m3.find()) {
                    formedString = "(" + DATE_COL + " >= \"" + currentYear + "-" + currentMonthNum + "-" + m3.group() + "\")";
                    //dayAndMounth = mather.group().split("\\.");
                    //formedString = "(" + DATE_COL + " >= \""+ currentYear + "-" + dayAndMounth[1] + "-" + dayAndMounth[0] + "\")";
                }
                break;

            case 7: //getBeforeDateSQL
                patterm = Pattern.compile("\\d{1,2}");
                //patterm = Pattern.compile("\\d{2}\\.\\d{2}");
                mather = patterm.matcher(partString);
                while (mather.find()) {
                    formedString = "(" + DATE_COL + " <= \"" + currentYear + "-" + currentMonthNum + "-" + mather.group() + "\")";
                    //dayAndMounth = mather.group().split("\\.");
                    //formedString = "(" + DATE_COL + " <= \"" + currentYear + "-" + dayAndMounth[1] + "-" + dayAndMounth[0] + "\")";
                }

                break;

            case 8: //getExceptDateSQL
                patterm = Pattern.compile("\\d{2}\\.\\d{2}");
                mather = patterm.matcher(partString);
                while (mather.find()) {
                    dayAndMounth = mather.group().split("\\.");
                    if (formedString.equals("")) {
                        formedString = "(" + DATE_COL + " != ";
                        formedString += "\"" + currentYear + "-" + dayAndMounth[1] + "-" + dayAndMounth[0] + "\"";
                    } else {
                        formedString += " AND " + DATE_COL + " != \"" + currentYear + "-" + dayAndMounth[1] + "-" + dayAndMounth[0] + "\"";
                    }
                }
                formedString += ")";
                break;

            case 9: //getExceptWeekDaySQL

                patterm = Pattern.compile("(пн|вт|ср|чт|пт|сб|вс)+");
                mather = patterm.matcher(partString);
                while (mather.find()) {
                    if (formedString.equals("")) {
                        formedString = "(" + "strftime('%w', " + DATE_COL + ") != \"" + map.get(mather.group()) + "\"";
                    } else {
                        formedString += " AND strftime('%w', " + DATE_COL + ") != \"" + map.get(mather.group()) + "\"";
                    }
                }
                formedString += ")";
                break;

            default:
                formedString = "(date('NOW') = date('1990.01.01'))";
                break;
        }

        if (isQueryStringEmpty())
            formedString = " WHERE " + formedString;
        else
            formedString = " AND " + formedString;

        appendQueryString(formedString);
        removePart(regExpr);
    }

}
