package com.gruk.yegorka.testviewpager.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;

import com.gruk.yegorka.testviewpager.R;
import com.gruk.yegorka.testviewpager.Util;
import com.gruk.yegorka.testviewpager.activities.ScheduleActivity;
import com.gruk.yegorka.testviewpager.adapters.TEST_RVA;
import com.gruk.yegorka.testviewpager.database.MyPrefs;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

import org.json.JSONException;

import java.util.Arrays;
import java.util.List;

public class FragmentStationDouble extends Fragment {

    public static final String TITLE = "По маршруту";//Util.getString(R.string.by_route);

    public static final String KEY = FragmentStationDouble.class.getName();

    private MaterialAutoCompleteTextView editStationFrom, editStationTo;
    private ProgressBar progressBar;

    ArrayAdapter<String> adapter;
    TEST_RVA adapterRecent;
    RecyclerView listView;
    ViewPager viewPager;

    public FragmentStationDouble() {
    }

    public static Fragment newInstance(int pos) {
        FragmentStationDouble fragment = new FragmentStationDouble();
        Bundle args = new Bundle();
        args.putInt(KEY, pos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        viewPager = (ViewPager) container;

        progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_spinner);

        final View view = inflater.inflate(R.layout.fragment_stations_double, container, false);

        listView = (RecyclerView) view.findViewById(R.id.list_view_recent);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(llm);

        adapterRecent = new TEST_RVA(getActivity(), Util.list2RecentModels(MyPrefs.getInstance().getList(KEY)));

        //adapterRecent = new AdapterListRecent(getActivity(), Util.set2RecentModels(MyPrefs.getInstance().getStringSet(KEY)));
        listView.setAdapter(adapterRecent);

        listView.setAdapter(adapterRecent);

        final String[] stations = MyPrefs.getInstance().getStringPrefs(MyPrefs.STATION_PREFS).split(MyPrefs.DIVIDER);
        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, stations);

        editStationFrom = (MaterialAutoCompleteTextView) view.findViewById(R.id.editText_from);
        editStationTo = (MaterialAutoCompleteTextView) view.findViewById(R.id.editText_to);

        editStationFrom.setAdapter(adapter);
        editStationTo.setAdapter(adapter);

        Button button = (Button) view.findViewById(R.id.button_show_rasp);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> stationsL = Arrays.asList(stations);
                String stationFrom = editStationFrom.getText().toString();
                String stationTo = editStationTo.getText().toString();

                if (stationFrom.trim().equals(stationTo.trim()) && !stationFrom.equals("")) {
                    editStationTo.setError("Станции отправления и прибытия совпадают");
                    editStationFrom.setError("Станции отправления и прибытия совпадают");
                } else if (stationsL.contains(stationFrom) &&
                        stationsL.contains(stationTo)) {
                    //MyPrefs.getInstance().addStringSet(KEY, stationFrom + MyPrefs.DIVIDER2 + stationTo);
                    MyPrefs.getInstance().add2List(KEY, stationFrom + MyPrefs.DIVIDER2 + stationTo);
                    startActivity(stationFrom, stationTo);

                } else {

                    if (!stationsL.contains(stationFrom)) {
                        if (stationFrom.equals(""))
                            editStationFrom.setError("Введите название станции");
                        else
                            editStationFrom.setError("Нет такой станции \"" + stationFrom + "\"");
                    }

                    if (!stationsL.contains(stationTo)) {
                        if (stationTo.equals(""))
                            editStationTo.setError("Введите название станции");
                        else
                            editStationTo.setError("Нет такой станции \"" + stationTo + "\"");
                    }

                }
            }
        });

        return view;
    }

    private void startActivity(String stationFrom, String stationTo) {
        Intent intent = new Intent(getActivity(), ScheduleActivity.class);
        intent.putExtra(MyPrefs.STATION_FROM, stationFrom);
        intent.putExtra(MyPrefs.STATION_TO, stationTo);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        //adapterRecent.setModels(Util.set2RecentModels(MyPrefs.getInstance().getStringSet(KEY));
        adapterRecent.setModels(Util.list2RecentModels(MyPrefs.getInstance().getList(KEY)));
    }

    public void switchVisibility() {
        if (progressBar.getVisibility() == View.INVISIBLE)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.INVISIBLE);
    }
}
