package com.gruk.yegorka.testviewpager.models;

import android.support.annotation.DrawableRes;
import android.support.annotation.IntegerRes;
import android.util.Log;

import com.gruk.yegorka.testviewpager.R;
import com.gruk.yegorka.testviewpager.Util;

import java.util.HashMap;
import java.util.Map;

public class DatabaseModel {

    private String arrival;
    private String departure;
    private String number;
    private String direction;
    private String days;

    private String trainType;

    @DrawableRes
    private int ImageId;

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public int getImageId() {
        return resMap.get(getTrainType());
    }

    public void setImageId(int imageId) {
        ImageId = imageId;
    }

    public String getTravelTime() {
        return Util.timeDifference(departure, arrival);
    }

    public String getTrainType() {
        return trainType;
    }

    public void setTrainType(String trainType) {
        this.trainType = trainType;
    }

    Map<String, Integer> resMap = new HashMap<String, Integer>(){{
        put(Util.getString(R.string.train_type_rle), R.drawable.rle);
        put(Util.getString(R.string.train_type_rlb), R.drawable.rlb);
        put(Util.getString(R.string.train_type_cl), R.drawable.cl);
    }};


}
