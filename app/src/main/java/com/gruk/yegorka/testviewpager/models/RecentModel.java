package com.gruk.yegorka.testviewpager.models;

public class RecentModel {

    private String station1;
    private String station2;

    public RecentModel(String station1, String station2) {
        this.station1 = station1;
        this.station2 = station2;
    }

    public String getStation1() {
        return station1;
    }

    public void setStation1(String station1) {
        this.station1 = station1;
    }

    public String getStation2() {
        return station2;
    }

    public void setStation2(String station2) {
        this.station2 = station2;
    }

}
