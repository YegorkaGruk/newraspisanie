package com.gruk.yegorka.testviewpager.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gruk.yegorka.testviewpager.MyAsyncTaskLoader;
import com.gruk.yegorka.testviewpager.R;
import com.gruk.yegorka.testviewpager.adapters.AdapterRecyclerView;
import com.gruk.yegorka.testviewpager.database.MyPrefs;
import com.gruk.yegorka.testviewpager.models.DatabaseModel;

import java.util.ArrayList;
import java.util.List;

public abstract class FragmentDay extends Fragment implements LoaderManager.LoaderCallbacks<List<DatabaseModel>>, View.OnClickListener {

    //private static final int URL_LOADER = 1;
    //private final int URL_LOADER = getLoaderId();

    private View emptyView;
    private RecyclerView rv;
    private AdapterRecyclerView adapterRecyclerView;

    public FragmentDay() {
    }

    private void setState(List<DatabaseModel> list2) {
        if (list2.isEmpty()) {
            rv.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            rv.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle bundle = getActivity().getIntent().getExtras();
        bundle.putBoolean(MyAsyncTaskLoader.IS_TODAY, isToday());

        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();

        if (bundle.getString(MyPrefs.STATION_TO) == null) {
            actionBar.setTitle(null);
            actionBar.setSubtitle(bundle.getString(MyPrefs.STATION_FROM)); //setSubtitle
        }
        else {
            actionBar.setTitle(null);
            actionBar.setSubtitle(bundle.getString(MyPrefs.STATION_FROM) + " - " + bundle.getString(MyPrefs.STATION_TO));
            //R.string.schedule
        }

        //TODO если одна станция, то другой layout
        View view = inflater.inflate(R.layout.fragment_days, container, false);

        rv = (RecyclerView) view.findViewById(R.id.rv);
        emptyView = view.findViewById(R.id.emptyLayout);

        emptyView.findViewById(R.id.back_btn).setOnClickListener(this);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        adapterRecyclerView = new AdapterRecyclerView(getContext(), new ArrayList<DatabaseModel>(0));
        rv.setAdapter(adapterRecyclerView);

        getActivity().getSupportLoaderManager().initLoader(getLoaderId(), bundle, this).forceLoad();

        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.back_btn:
                getActivity().finish();
                break;
        }

    }

    @Override
    public Loader<List<DatabaseModel>> onCreateLoader(int loaderID, Bundle args) {
        //switch (loaderID) {
         //   case getLoaderId();
                return new MyAsyncTaskLoader(getActivity(), args);
          //  default:
          //      return null;
        //}
    }

    @Override
    public void onLoadFinished(Loader<List<DatabaseModel>> loader, List<DatabaseModel> data) {
        adapterRecyclerView.setModels(data);
        setState(data);
    }

    @Override
    public void onLoaderReset(Loader<List<DatabaseModel>> loader) {

    }

    protected abstract boolean isToday();
    protected abstract int getLoaderId();

}
