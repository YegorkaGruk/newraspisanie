package com.gruk.yegorka.testviewpager.database;

import java.util.ArrayList;

class MyArrayList<T> extends ArrayList<String[]> {

    public MyArrayList() {
        super();
    }

    public MyArrayList(int size) {
        super(size);
    }

    @Override
    public int indexOf(Object arg0) {

        String[] a = (String[]) arg0;

        for (int i = size() - 1; i >= 0; i--)
            if (get(i)[0].equals(a[0]) && get(i)[1].equals(a[1]))
                return i;

        return -1;
    }
}