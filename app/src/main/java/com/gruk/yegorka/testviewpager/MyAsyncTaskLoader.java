package com.gruk.yegorka.testviewpager;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.gruk.yegorka.testviewpager.database.MyPrefs;
import com.gruk.yegorka.testviewpager.database.MySQLiteClassHolder;
import com.gruk.yegorka.testviewpager.fragments.FragmentDayEveryDay;
import com.gruk.yegorka.testviewpager.models.DatabaseModel;

import java.util.List;

public class MyAsyncTaskLoader extends AsyncTaskLoader<List<DatabaseModel>> {

    public static final String IS_TODAY = "IS_TODAY";

    SetType type;
    String station1, station2;

    public MyAsyncTaskLoader(Context context, Bundle bundle) {
        super(context);

        boolean today = bundle.getBoolean(IS_TODAY);
        station1 = bundle.getString(MyPrefs.STATION_FROM);
        station2 = bundle.getString(MyPrefs.STATION_TO);

        Log.e("[MyAsyncTaskLoader]", "today = " + today + " station1 - " + station1 + " station2 - " + station2);

        if (station2 != null) {
            if (!today)
                type = SetType.ROUTE;
            else
                type = SetType.ROUTE_NOW;
        } else {
            if (!today)
                type = SetType.STATION;
            else
                type = SetType.STATION_NOW;
        }

        Log.e("[MyAsyncTaskLoader]", "type = " + type);

    }

    @Override
    public List<DatabaseModel> loadInBackground() {

        switch (type) {
            case ROUTE:
                return MySQLiteClassHolder.getInstance().getScheduleRoute(station1, station2);
            case ROUTE_NOW:
                return MySQLiteClassHolder.getInstance().getScheduleRouteNow(station1, station2);
            case STATION:
                return MySQLiteClassHolder.getInstance().getScheduleStation(station1);
            case STATION_NOW:
                return MySQLiteClassHolder.getInstance().getScheduleStationNow(station1);
        }

        return null;
    }

    public enum SetType {
        ROUTE, ROUTE_NOW, STATION, STATION_NOW;
    }
}
