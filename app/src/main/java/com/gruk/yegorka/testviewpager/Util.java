package com.gruk.yegorka.testviewpager;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.annotation.ArrayRes;
import android.support.annotation.StringRes;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.gruk.yegorka.testviewpager.database.MyPrefs;
import com.gruk.yegorka.testviewpager.models.NotificationModel;
import com.gruk.yegorka.testviewpager.models.RecentModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Util {

    public static String getString(@StringRes int id) {
        return App.getContext().getString(id);
    }

    public static String[] getStringArray(@ArrayRes int id) {
        return App.getContext().getResources().getStringArray(id);
    }

    public static String timeDifference(String departure, String arrival) {

        String hours, minutes;

        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date date1 = null, date2 = null;
        try {
            date1 = format.parse(departure);
            date2 = format.parse(arrival);
        } catch (ParseException e) {
            Log.e("Util.timeDifference", e.getMessage());
            throw new RuntimeException();
        }

        long difference = TimeUnit.MILLISECONDS.toMinutes(date2.getTime() - date1.getTime());

        hours = String.valueOf(difference / 60);
        minutes = String.valueOf(difference % 60);

        return hours + " ч : " + minutes + " мин";
    }

    public static String[] getTrainTypes() {

        Set<String> stringSet = App.getContext()
                .getSharedPreferences(MyPrefs.APP_PREFS, Context.MODE_PRIVATE)
                .getStringSet(getString(R.string.selected_train_types), new LinkedHashSet<String>());
        //Set<String> stringSet = MyPrefs.getInstance().getStringSet(getString(R.string.selected_train_types));

        if (stringSet == null)
            return App.getContext().getResources().getStringArray(R.array.preference_list_entries_default);
        else
            return stringSet.toArray(new String[]{});

    }

    public static List<RecentModel> set2RecentModels(Set<String> set) {

        List<RecentModel> list = new ArrayList<>(set.size());

        for (String s : set) {
            String[] strings1 = s.split(MyPrefs.DIVIDER2);
            list.add(new RecentModel(strings1[0], strings1[1]));
            Log.d("s.split", strings1[0] + " | " + strings1[1]);
        }

        return list;
    }

    public static List<RecentModel> list2RecentModels(List<String> list1) {

        List<RecentModel> list = new ArrayList<>(list1.size());

        for (String s : list1) {
            String[] strings1 = s.split(MyPrefs.DIVIDER2);
            list.add(new RecentModel(strings1[0], strings1[1]));
            Log.d("s.split", strings1[0] + " | " + strings1[1]);
        }

        return list;
    }

    public static List<String> JSONArray2List(JSONArray jsonArray) throws JSONException {
        List<String> list = new LinkedList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            list.add(jsonArray.getString(i));
        }

        return list;
    }

    //TODO подредактировать
    public static Set<String> funnyAdd(Set<String> set, String adding) {

        LinkedList<String> strings = new LinkedList<>(set);

        if (strings.contains(adding))
            strings.remove(adding);

        strings.addFirst(adding);

        return new LinkedHashSet<>(strings);
    }

    public static void sendNotification(NotificationModel model) {

        NotificationManager nm = (NotificationManager) model.getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent pendingIntent = PendingIntent.getActivity(
                model.getContext(),
                model.getRequestCode(),
                new Intent(model.getContext(), model.getClazz()),
                PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(model.getContext());

        builder.setAutoCancel(model.isAutoCancel());
        builder.setTicker(model.getTicker());
        builder.setContentTitle(model.getContentTitle());
        builder.setContentText(model.getContentText());
        builder.setSmallIcon(model.getSmallIcon());
        builder.setContentIntent(pendingIntent);
        builder.setOngoing(model.isOngoing());
        builder.setSubText(model.getSubText());   //API level 16
        builder.setNumber(model.getNumber());
        builder.setWhen(model.getWhen());
        builder.setSound(model.getSoundUri());

        Notification notification = builder.build();//getNotification
        nm.notify(model.getId(), notification);

        wakeUpFor(10);
    }

    public static void wakeUpFor(int seconds) {
        PowerManager pm = (PowerManager) App.getContext().getSystemService(Context.POWER_SERVICE);
        if (!pm.isScreenOn()) {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "MyLock");
            wl.acquire(seconds * 1000);
        }
    }

}
