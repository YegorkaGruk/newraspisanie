package com.gruk.yegorka.testviewpager.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.gruk.yegorka.testviewpager.R;
import com.gruk.yegorka.testviewpager.Util;
import com.gruk.yegorka.testviewpager.models.DatabaseModel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MySQLiteClass {

    private static final String TAG = MySQLiteClass.class.getName();

    private static final String DATABASE_NAME = "timetable_Db";
    private static final int DATABASE_VERSION = 1;

    private static final String SCHEDULE_TABLE_DEPARTURE = "table_departure";
    private static final String SCHEDULE_TABLE_ARRIVAL = "table_arrival";
    private static final String TRAIN_TABLE = "train_table";

    private static final String TRAIN_ID = "tr_id";
    private static final String TRAIN_NUMBER = "train_number";
    private static final String TRAIN_DIRECTION = "train_direction";
    private static final String TRAIN_DESCRIPTION = "train_description";
    private static final String TRAIN_DAYS = "train_days";
    private static final String TRAIN_DAYS_SQL_QUERY = "train_query";
    //private static final String TRAIN_CLASS = "train_class";

    private static final String SCHEDULE_ID = "sh_id";

    private static final String SET_CHECK = "SELECT \"whatever\"";
    private static final String STAT_PREFS = "station_prefs";

    private Context context;
    private DBHelp dbhelp;
    private SQLiteDatabase thisDataBase;
    private ParserCombinator combinator;

    public MySQLiteClass(Context context) {
        this.context = context;
        combinator = new ParserCombinator();
    }

    public static void copyDb(Context cont) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

            if (sd.canWrite()) {
                String packageName = cont.getPackageName();
                String currentDBPath = "//data//" + packageName + "//databases//" + DATABASE_NAME + "";
                String backupDBPath = DATABASE_NAME;
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
            Log.d("copyDb", "coped");
        } catch (Exception e) {
            Log.d("copyDb", "exception");
            e.printStackTrace();
        }
    }

    public MySQLiteClass open(boolean writable) throws SQLiteException {

        dbhelp = new DBHelp(context);

        if (writable)
            thisDataBase = dbhelp.getWritableDatabase();
        else
            thisDataBase = dbhelp.getReadableDatabase();
        return this;
    }

    public void close() {
        dbhelp.close();
    }

    private int addTrainUnsafe(String id, String direction, String description, String days, String query) {

        ContentValues values = new ContentValues();

        values.put(TRAIN_NUMBER, id);
        values.put(TRAIN_DIRECTION, direction);
        values.put(TRAIN_DESCRIPTION, description);
        values.put(TRAIN_DAYS, days);
        values.put(TRAIN_DAYS_SQL_QUERY, query);

        int currentRow = (int) thisDataBase.insert(TRAIN_TABLE, null, values);

        values.clear();
        values.put(SCHEDULE_ID, currentRow);
        thisDataBase.insert(SCHEDULE_TABLE_ARRIVAL, null, values);
        thisDataBase.insert(SCHEDULE_TABLE_DEPARTURE, null, values);

        return currentRow;
    }

    private void addArrivalTimeUnsafe(int trainId, String columnName, String arrivalTime, String departureTime) {

        ContentValues values = new ContentValues();

        values.put("\"" + columnName + "\"", departureTime);
        thisDataBase.update(SCHEDULE_TABLE_DEPARTURE, values, SCHEDULE_ID + " = " + trainId, null);

        values.clear();
        values.put("\"" + columnName + "\"", arrivalTime);
        thisDataBase.update(SCHEDULE_TABLE_ARRIVAL, values, SCHEDULE_ID + " = " + trainId, null);

    }

    private int isTrainExistUnsafe(String tableName, String trainNum, String direct, String days) {

        Cursor c = thisDataBase.rawQuery(
                "SELECT " + TRAIN_ID +
                        " FROM " + tableName +
                        " WHERE " +
                        TRAIN_NUMBER + " = ? AND " +
                        TRAIN_DIRECTION + " = ? AND " +
                        TRAIN_DAYS + " = ?",
                new String[]{trainNum, direct, days});

        int trainId = -1;

        if (c.moveToFirst())
            trainId = c.getInt(c.getColumnIndex(TRAIN_ID));

        c.close();

        return trainId;
    }

    private void addColumnUnsafe(String tableName, String columnName) {
        thisDataBase.execSQL("ALTER TABLE " + tableName + " ADD COLUMN \"" + columnName + "\" TEXT");
    }

    private void dropTables() {

        open(true);

        thisDataBase.execSQL("DROP TABLE" + SCHEDULE_TABLE_ARRIVAL);
        thisDataBase.execSQL("DROP TABLE" + SCHEDULE_TABLE_DEPARTURE);
        thisDataBase.execSQL("DROP TABLE" + TRAIN_TABLE);

        close();
    }

    public ArrayList<DatabaseModel> getScheduleRoute(String station1, String station2) {

        String set =
                "   SELECT D.[" + station1 + "] as departure, D.[" + station2 + "] as arrival,\n" +
                        "   T.train_number, T.train_direction, T.train_description, T.train_days, T.train_query\n" +
                        "   FROM table_arrival A, table_departure D, train_table T\n" +
                        "   WHERE A.sh_id = D.sh_id \n" +
                        "   AND D.sh_id = T.tr_id\n" +
                        "   AND D.[" + station1 + "] NOT NULL\n" +
                        "   AND D.[" + station2 + "] NOT NULL\n" +
                        "   AND time(D.[" + station1 + "]) < time(D.[" + station2 + "])\n" +
                        "   ORDER BY D.[" + station2 + "]";

        return getData(set, false);
    }

    public ArrayList<DatabaseModel> getScheduleRouteNow(String station1, String station2) {

        String set =
                "   SELECT A.[" + station1 + "] as departure, D.[" + station2 + "] as arrival,\n" +
                        "   T.train_number, T.train_direction, T.train_description, T.train_days, T.train_query\n" +
                        "   FROM table_arrival A, table_departure D, train_table T\n" +
                        "   WHERE A.sh_id = D.sh_id \n" +
                        "   AND A.sh_id = T.tr_id\n" +
                        "   AND A.[" + station1 + "] NOT NULL\n" +
                        "   AND D.[" + station2 + "] NOT NULL\n" +
                        "   AND time(A.[" + station1 + "]) < time(D.[" + station2 + "])\n" +
                        "   AND time(A.[" + station1 + "]) > time('NOW','localtime')\n" +
                        "   ORDER BY D.[" + station2 + "]";

        return getData(set, true);
    }

    public ArrayList<DatabaseModel> getScheduleStation(String station) {

        String set =
                "   SELECT D.[" + station + "] as departure,  A.[" + station + "] as arrival,\n" +
                        "   T.train_number, T.train_direction, T.train_description, T.train_days, T.train_query\n" +
                        "   FROM table_arrival A, table_departure D, train_table T\n" +
                        "   WHERE D.sh_id = T.tr_id    \n" +
                        "   AND D.sh_id = A.sh_id\n" +
                        "   AND A.[" + station + "] NOT NULL\n" +
                        "   AND A.[" + station + "] != ''\n" +
                        "   ORDER BY A.[" + station + "]";

        return getData(set, false);
    }

    public ArrayList<DatabaseModel> getScheduleStationNow(String station) {

        String set =
                "   SELECT D.[" + station + "] as departure,  A.[" + station + "] as arrival,\n" +
                        "   T.train_number, T.train_direction, T.train_description, T.train_days, T.train_query\n" +
                        "   FROM table_arrival A, table_departure D, train_table T\n" +
                        "   WHERE D.sh_id = T.tr_id\n" +
                        "   AND D.sh_id = A.sh_id\n" +
                        "   AND A.[" + station + "] NOT NULL\n" +
                        "   AND A.[" + station + "] != ''\n" +
                        "   AND time(A.[" + station + "]) > time('NOW','localtime')\n" +
                        "   ORDER BY A.[" + station + "]";

        return getData(set, true);
    }

    private ArrayList<DatabaseModel> getData(String set, boolean atThisTime) {

        ArrayList<DatabaseModel> models = new ArrayList<>();

        try {

            Cursor c = thisDataBase.rawQuery(set, null);

            Cursor innerCursor;
            String arrival, departure, query;

            while (c.moveToNext()) {

                DatabaseModel databaseModel = new DatabaseModel();

                arrival = c.getString(c.getColumnIndex("arrival"));
                departure = c.getString(c.getColumnIndex("departure"));

                if (arrival.equals(""))
                    arrival = context.getString(R.string.stop_station);

                if (departure.equals(""))
                    departure = context.getString(R.string.start_station);

                databaseModel.setArrival(arrival);
                databaseModel.setDeparture(departure);
                databaseModel.setNumber(c.getString(c.getColumnIndex(TRAIN_NUMBER)));
                databaseModel.setDirection(c.getString(c.getColumnIndex(TRAIN_DIRECTION)));
                databaseModel.setTrainType(c.getString(c.getColumnIndex(TRAIN_DESCRIPTION)));
                databaseModel.setDays(c.getString(c.getColumnIndex(TRAIN_DAYS)));


                if (!atThisTime)
                    models.add(databaseModel);
                else {

                    query = c.getString(c.getColumnIndex(TRAIN_DAYS_SQL_QUERY));
                    innerCursor = thisDataBase.rawQuery(SET_CHECK + query, null);

                    while (innerCursor.moveToNext())
                        models.add(databaseModel);

                    innerCursor.close();
                }

            }

            c.close();

        } catch (SQLiteException e) {
            Log.e("getData()", e.getMessage());
            throw new RuntimeException();
        }

        return models;
    }

    public void initialFillingSafe(Resources resources) {

        open(true);

        String prefix = "http://rasp.rw.by/ru/station/?station=";
        String postfix = "&date=everyday";
        String[] stationNames = resources.getStringArray(R.array.stations);

        System.setProperty("http.agent", "");
        resetDbUnsafe();

        Log.d(TAG, " downloading start");

        String encodedStationName;
        Document webpageDocument = null;
        Elements idElem, arrivalElem, departureElem, daysElem, directionElem, descriptionElem;
        List<String> trainTypes = Arrays.asList(Util.getTrainTypes());

        for (String currentStation : stationNames) {

            try {
                encodedStationName = URLEncoder.encode(currentStation, "UTF-8");
                webpageDocument = Jsoup.connect(prefix + encodedStationName + postfix).userAgent("IE/9.0").timeout(25 * 1000).get();
            } catch (IOException e) {
                Log.e("rrr", e.getMessage());
                e.printStackTrace();
            }

            idElem = webpageDocument.body().getElementsByClass("train_id");
            arrivalElem = webpageDocument.body().getElementsByClass("train_start-time");
            departureElem = webpageDocument.body().getElementsByClass("train_end-time");
            daysElem = webpageDocument.body().getElementsByClass("train_halts");
            directionElem = webpageDocument.body().getElementsByClass("train_text");
            descriptionElem = webpageDocument.body().getElementsByClass("train_description");


            addColumnUnsafe(SCHEDULE_TABLE_ARRIVAL, currentStation);
            addColumnUnsafe(SCHEDULE_TABLE_DEPARTURE, currentStation);

            for (int i = 0; i < idElem.size(); i++) {

                String id = idElem.get(i).text();
                String arrival = arrivalElem.get(i).text();
                String departure = departureElem.get(i).text();
                String days = daysElem.get(i).text();
                String direction = directionElem.get(i).text();
                String description = descriptionElem.get(i).text();

                if (!trainTypes.contains(description))
                    continue;

                int trainId = isTrainExistUnsafe(TRAIN_TABLE, id, direction, days);

                if (trainId == -1) {
                    trainId = addTrainUnsafe(id, direction, description, days, combinator.getFormedQuery(days));
                }
                addArrivalTimeUnsafe(trainId, currentStation, arrival, departure);

            }
        }

        StringBuilder sb = new StringBuilder();
        for (String s : stationNames) {
            sb.append(s).append(",");
        }

        MyPrefs.getInstance().setPrefs(STAT_PREFS, sb.toString());

        close();
        Log.d(TAG, "downloading finally finished");
    }

    private void resetDbUnsafe() {
        thisDataBase.execSQL("DROP TABLE IF EXISTS " + SCHEDULE_TABLE_ARRIVAL);
        thisDataBase.execSQL("DROP TABLE IF EXISTS " + SCHEDULE_TABLE_DEPARTURE);
        thisDataBase.execSQL("DROP TABLE IF EXISTS " + TRAIN_TABLE);

        thisDataBase.execSQL(DBHelp.CREATE_TRAIN_TABLE);
        thisDataBase.execSQL(DBHelp.CREATE_ARRIVAL_TABLE);
        thisDataBase.execSQL(DBHelp.CREATE_DEPARTURE_TABLE);
    }

    private class DBHelp extends SQLiteOpenHelper {
        public static final String CREATE_TRAIN_TABLE =
                "CREATE TABLE " + TRAIN_TABLE + "(" +
                        TRAIN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        TRAIN_NUMBER + " TEXT NOT NULL, " +
                        TRAIN_DIRECTION + " TEXT NOT NULL, " +
                        TRAIN_DESCRIPTION + " TEXT NOT NULL, " +
                        TRAIN_DAYS + " TEXT NOT NULL, " +
                        TRAIN_DAYS_SQL_QUERY + " TEXT NOT NULL);";

        public static final String CREATE_ARRIVAL_TABLE =
                "CREATE TABLE " + SCHEDULE_TABLE_ARRIVAL + "(" +
                        SCHEDULE_ID + " INTEGER, " +
                        "FOREIGN KEY(" + SCHEDULE_ID + ") " +
                        "REFERENCES " + TRAIN_TABLE + "(" + TRAIN_ID + "));";

        public static final String CREATE_DEPARTURE_TABLE =
                "CREATE TABLE " + SCHEDULE_TABLE_DEPARTURE + "(" +
                        SCHEDULE_ID + " INTEGER, " +
                        "FOREIGN KEY(" + SCHEDULE_ID + ") " +
                        "REFERENCES " + TRAIN_TABLE + "(" + TRAIN_ID + "));";

        public DBHelp(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TRAIN_TABLE);
            db.execSQL(CREATE_ARRIVAL_TABLE);
            db.execSQL(CREATE_DEPARTURE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int old_version, int new_version) {
            dropTables();
            onCreate(db);
        }
    }
}