package com.gruk.yegorka.testviewpager.database;

import android.content.Context;

import com.gruk.yegorka.testviewpager.App;

public class MySQLiteClassHolder {

    private static volatile MySQLiteClass instance;

    public static MySQLiteClass getInstance() {
        MySQLiteClass localInstance = instance;
        if (localInstance == null) {
            synchronized (MySQLiteClass.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new MySQLiteClass(App.getContext());
                }
            }
        }
        return localInstance;
    }

}
