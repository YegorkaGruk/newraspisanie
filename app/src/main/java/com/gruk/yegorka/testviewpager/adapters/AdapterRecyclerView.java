package com.gruk.yegorka.testviewpager.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.gruk.yegorka.testviewpager.R;
import com.gruk.yegorka.testviewpager.models.DatabaseModel;

import java.util.List;

public class AdapterRecyclerView extends RecyclerView.Adapter<AdapterRecyclerView.ViewHolder> {

    private List<DatabaseModel> models;

    private Context context;
    // Allows to remember the last item shown on screen
    private int lastPosition = -1;

    public AdapterRecyclerView(Context context, List<DatabaseModel> models) {
        this.context = context;
        this.models = models;
    }

    public void setModels(List<DatabaseModel> models) {
        this.models = models;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final DatabaseModel model = models.get(position);

        holder.iconType.setImageResource(model.getImageId());

        holder.trainNumber.setText(model.getNumber());
        holder.trainDirection.setText(model.getDirection());

        holder.arrivalTime.setText(model.getArrival());
        holder.travelTime.setText("travT");//model.getTravelTime());
        holder.departureTime.setText(model.getDeparture());

        // Here you apply the animation when the view is bound
        setAnimation(holder.cv, position);
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        CardView cv;

        ImageView iconType;
        TextView trainNumber;
        TextView trainDirection;

        TextView arrivalTime;
        TextView travelTime;
        TextView departureTime;

        ViewHolder(View itemView) {
            super(itemView);

            cv = (CardView) itemView.findViewById(R.id.cv);
            //--------------------------------------------------------------------------------------
            iconType = (ImageView) itemView.findViewById(R.id.icon_type);
            trainNumber = (TextView) itemView.findViewById(R.id.train_number);
            trainDirection = (TextView) itemView.findViewById(R.id.train_direction);

            arrivalTime = (TextView) itemView.findViewById(R.id.arrival_time);
            travelTime = (TextView) itemView.findViewById(R.id.travel_time);
            departureTime = (TextView) itemView.findViewById(R.id.departure_time);

        }
    }

}
